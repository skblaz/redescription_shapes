## This is the main python code for the redescription shape algorihm.


## read a csv,
## select subsets of columns and the target column(s)
## do this for each timestamp
## obtain temporal visualization by maximizing the likelihood for explanation of the target variable.

import numpy as np
from scipy.stats.stats import pearsonr
import pandas as pd
import matplotlib.pyplot as plt
from plotnine import *
from random import randint
from sklearn.model_selection import train_test_split
from sklearn import svm

def correlation(feature,target):
    return pearsonr(feature,target)[1]

def svm_prediction(feature,target):
    try:
        X_train, X_test, y_train, y_test = train_test_split(feature, target, test_size=0.5, random_state=1254)
        clf = svm.SVC(kernel='linear', C=1).fit(X_train, y_train)
        return clf.score(X_test, y_test)
    except:
        return 0

def get_max_feature(scoring_functions, column_names, dataframe, target_variable, ts = 1):
    representatives = []    
    for k,v in column_names.items():
        subset = dataframe[v]
        local_max = 0
        for column in subset:
            score = scoring_functions[1](subset,dataframe[target_variable])
            if score > local_max:
                rcol = column
                local_max = score

        representatives.append({"ptime":ts,"group" : k,"score":local_max,"representative_feature" : rcol})
    return representatives

if __name__ == "__main__":

    from sklearn import preprocessing
    le = preprocessing.LabelEncoder()
    lb = preprocessing.LabelBinarizer()
    rframe = pd.DataFrame(columns=["ptime","group","score","representative_feature"])
    dataframe = pd.read_csv("data/EXXPORTED.csv").fillna(0)
    dataframe = dataframe[dataframe.target != 0]
    print(dataframe.columns)
    
    tx = list(dataframe["target"])
    converted = lb.fit_transform(tx)
    dataframe.target = converted
#    dataframe.to_csv("data/train_test.csv")
#    dataframe["ptime"] = [randint(0,10) for x in range(dataframe.shape[0])]

    cnames = dataframe.columns.values
    group_mapping = {"g1" : cnames[2:5],
                     "g2" : cnames[5:8],
                     "g3" : cnames[8:12],
                     "g4" : cnames[12:15],
                     "g5" : cnames[15:18],
                     "env" : cnames[20:]}

    scoring_functions = [correlation,svm_prediction]
    for timestamp in dataframe.YEAR.unique():
        subset = dataframe[dataframe["YEAR"] == timestamp]
        reps = get_max_feature(scoring_functions, group_mapping,subset, "target", ts=timestamp)
        for x in reps:
            rframe = rframe.append(x,ignore_index=True)

    print(rframe)
    gx = (ggplot(rframe, aes(x='ptime', y='score'))
          + geom_bar(aes(fill='group'),position='dodge',stat='identity') + theme_classic(base_size=20) + xlab("Timestamp") + ylab("Score")
    )
    gx.draw()
    plt.show()
