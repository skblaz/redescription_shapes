## enostavni primer predikcij
## by. Blaz Skrlj

import pandas as pd
import numpy as np
from collections import defaultdict
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import (RandomTreesEmbedding, RandomForestClassifier,GradientBoostingClassifier)
from sklearn.tree import DecisionTreeClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
import sklearn.svm
from sklearn.dummy import DummyClassifier

def train_test_describe(dataset,dname):

    """
    Opis mnozic za ucenje.
    """
    
    positives = dataset[dataset.status == 1].shape[0]
    negatives = dataset[dataset.status == 0].shape[0]
    print("{}: positive targets: {}, negative targets: {}".format(dname, positives, negatives))
    
def evaluate_learners(df, learners,train_years,test_years):

    """
    Evaluacijska funkcija. Za vsak klasifikator pogledamo, kako dobro znamo napovedovati. Vedno uporabimo podatke iz preteklosti za napoved prihodnosti!
    """
    
    learner_scores = []
    
    trainset = df[df['year'].isin(train_years)]
    testset = df[df['year'].isin(test_years)]

    train_test_describe(trainset,"train set")
    train_test_describe(testset,"test set")
    
    train_target = trainset['status']
    train_train = trainset.drop(['status','year','plant'],axis=1).fillna(df.min())

    test_target = testset['status']
    test_train = testset.drop(['status','year','plant'],axis=1).fillna(df.mean())    
    
    for name,clf in learners.items():
        try:
            clf.fit(train_train,train_target)
            pred = clf.predict(test_train)
            score = sklearn.metrics.f1_score(test_target, pred, average='binary')
            acc = sklearn.metrics.accuracy_score(test_target, pred)
            learner_scores.append({"learner" : name,"accuracy":acc,"F1":score,'classifier_object' : clf})
        except:
            print("Classifier {} failed.".format(name))

    return (train_target,train_train,test_target,test_train,learner_scores)


def construct_learner_set():

    """
    Ta funkcija vsebuje set klasifikatorjev, ki nas zanima. Tukaj smo uporabili privzete nastavitve---vsak klasifikator lahko dodatno navijemo!
    """
    
    classifiers = {}
    
    ## to bi bilo fino stestirati
#    classifiers["XGB"] = XGBClassifier(max_depth=50,estimators=200,learning_rate=0.001)

    classifiers["RF"] = sklearn.ensemble.RandomForestClassifier(n_estimators=200)
    classifiers["MLP"] = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(2,2), random_state=1)
    classifiers["GBM"] = GradientBoostingClassifier(n_estimators=300)
    classifiers["DT"] = DecisionTreeClassifier(random_state=0)
    classifiers["SVM"] = sklearn.svm.SVC()
    classifiers["Dummy"] = DummyClassifier()
    classifiers["LDA"] = LinearDiscriminantAnalysis()
    classifiers["QDA"] = QuadraticDiscriminantAnalysis()

    ## to bi bilo fino stestirati
#    classifiers['autoML'] = autosklearn.classification.AutoSklearnClassifier()

    return classifiers
def explain_predictions( tta, ttr, tet, tetr, classification_results):

    fnames = list(ttr.columns) ## feature names

    ## instanca razlagalca
    explainer = lime.lime_tabular.LimeTabularExplainer(ttr, feature_names=fnames, class_names=["Healthy","Disease"], discretize_continuous=False)
    print("Features used:", fnames)
    to_check = []
    for ind, x in enumerate(list(tet)):
        to_check.append(ind)

    aggregates_tmp = defaultdict(list)
    aggregates_positives_tmp = defaultdict(list)
    aggregates = {}
    aggregates_positives = {}
    for trained_classifier in classification_results:
        classifier_name = trained_classifier['learner']
        clf = trained_classifier['classifier_object']
        predictions = clf.predict(tetr)
#        print(len(predictions),tet.shape)
        for j in to_check:
            try:
                prediction = predictions[j]
                real = tet.iloc[j]
                if real == prediction and real == 1:
                    
                    exp = explainer.explain_instance(tetr.iloc[j], clf.predict_proba, num_features=10, top_labels=1)
                    for x,y in exp.as_list(label=1):
                        try:
                            if x not in aggregates:
                                aggregates_tmp[x] = [y]
                            else:
                                aggregates_tmp[x].append(y)
                        except:
                            pass

                    exp.save_to_file("results/checks/"+str(real)+"_example_"+trained_classifier['learner']+"_example_"+str(j)+".html")
            except Exception as es:
                print(es)
                pass
            try:        
                if real == prediction and real == 0:
                    
                    exp = explainer.explain_instance(tetr.iloc[j], clf.predict_proba, num_features=10, top_labels=1)
                    for x,y in exp.as_list(label=0):
                        try:
                            if x not in aggregates:
                                aggregates_positives_tmp[x] = y
                            else:
                                aggregates_positives_tmp[x].append(y)
                        except:
                            pass
            except:
                print ("Could not explain {}".format(trained_classifier['learner']))
                pass

        for k,v in aggregates_positives_tmp.items():
            aggregates_positives[k] = np.mean(v)
        for k,v in aggregates_tmp.items():
            aggregates[k] = np.mean(v)
            
        dfx = pd.DataFrame()
        dfx = dfx.append(aggregates,ignore_index=True).T.reset_index()
        dfx.columns = ["gene","Importance for disease development"]
        dfx['phenotype'] = "Disease"
        
        dfx2 = pd.DataFrame()
        dfx2 = dfx2.append(aggregates_positives,ignore_index=True).T.reset_index()
        dfx2.columns = ["gene","Importance for disease development"]
        dfx2['phenotype'] = "Healthy"

        dfx_final = pd.concat([dfx,dfx2],axis=0)
        return dfx_final
    
#        print(dfx_final)

        ## odkomentiraj za plottanje
        # dfx_final=dfx_final.rename(columns = {'phenotype':'Status'})
        # gx = (ggplot(dfx_final, aes('gene', 'Importance for disease development',fill="Status"))
        #       + geom_bar(stat="identity") + theme_classic()
        #       +xlab("Gene")
        #       +ylab("Average importance")
        #       +ggtitle("Feature importances for {}".format(classifier_name))
        #       + theme_bw()
        #       + theme(axis_text_x=element_text(rotation=90, hjust=1))
        #       + theme(axis_text=element_text(size=12))
        #       + theme(axis_title=element_text(size=15))
        # )
        # gx.draw()
        # if classifier_name == "GBM":
        #     plt.show()

            
if __name__ == "__main__":

    ## primer na lime razlagalcu
    import lime
    import lime.lime_tabular

    
    podatki = pd.read_csv("datasets/trte.csv")

    ## hitri pregled
    print(podatki.head())

    ## ostnovne statistike
    print(podatki.describe())

    ## leto 4 smo izkljucili, saj gre za ponovljen poskus leta 2008 (spomlad)
    print(np.unique(podatki['year']))
    
    """
    Leta so zakodirana takole:
    #1 -> 2004, 2->2005, 3->2007,4->2008_1,5->2008_2,6->2009_1 
    """

    ## najprej naredimo set klasifikatorjev
    classifier_set = construct_learner_set()

    ## evaluiramo in si zapomnimo modele
    tta, ttr, tet, tetr, results_of_classification = evaluate_learners(podatki, classifier_set,train_years=[1,2,3,4],test_years=[5,6])

    ## naredimo tabelo z rezultati klasifikacije
    edf = pd.DataFrame()
    edf = edf.append(results_of_classification,ignore_index=True)
    edf.drop(columns=["classifier_object"])
    df = edf.sort_values(by=['F1'])
    print(edf)
    explanations = explain_predictions( tta, ttr, tet, tetr, results_of_classification)

    ## na podlagi razlag pogledamo interakcije!
