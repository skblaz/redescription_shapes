
import glob2
import pandas as pd
from collections import defaultdict
import numpy as np
import lime
import lime.lime_tabular
import sklearn
import numpy as np
import sklearn.ensemble
import sklearn.metrics
import sklearn.datasets
#import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import (RandomTreesEmbedding, RandomForestClassifier,
                              GradientBoostingClassifier)
#from xgboost import XGBClassifier
#import autosklearn.classification
from sklearn.tree import DecisionTreeClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
import sklearn.svm
from sklearn.dummy import DummyClassifier
from plotnine import *
import matplotlib.pyplot as plt

## generate time series embeddings
def embed_ts(inseries,dimension,colnames=None):

    ## apply 0 index correction
    dimension+=1
    
    ## take a time series and craete a new pandas dataframe
    ## up to n dimensions    
    if colnames == None:
        colnames = ["embedding_"+str(step) for step in range(0,dimension)]
    finalFrame = pd.DataFrame(columns=colnames)
    tmpFrame = inseries.iloc[dimension:]

    for j in tmpFrame.index.tolist():
        embedding = []
        for tau in range(0,dimension):
            embedding.append(inseries.iloc[j-tau])
        data = dict(zip(colnames,embedding))
        finalFrame = finalFrame.append(data,ignore_index=True)
    return finalFrame

def parse_datasets(df_folder_list):

    dfx = []
    #1 -> 2004, 2->2005, 3->2007,4->2008_1,5->2008_2,6->2009_1
    common_columns = []
    for filename in df_folder_list:
        year = filename.split("/")[-1].split(".")[0]
        df = pd.read_csv(filename)
        if year != "4":
            df['year'] = year
            dfx.append(df)
            common_columns.append(list(df.columns))
    
    result = set.intersection(*map(set,common_columns))
    common_terms = []
    tabu_columns = ["Unnamed: 1","Unnamed: 2","symptoms","symptoms, discretized"]
    final_cols = [x for x in result if x not in tabu_columns]
    finalframe = pd.DataFrame()
    
    for x in dfx:
        try:
            subset = x[final_cols]
            finalframe = finalframe.append(subset)
        except:
            print("Failed conversion-reduction step")
            
    state_map = finalframe[['plant','year','status']]
    grouped = finalframe.groupby(["plant","year"]).mean()
    finalframe = grouped.reset_index()
    merged_frame = pd.merge(state_map,finalframe,how='left',on=["plant","year"]).sort_values("year")
    merged_frame['status'] = merged_frame['status'].replace(['I'],1)
    merged_frame['status'] = merged_frame['status'].replace(['H'],0)
    merged_frame['year'] = merged_frame['year'].astype(int)

    
    print(merged_frame['year'].unique())
    return merged_frame
    
def construct_learner_set():

    classifiers = {}
#    classifiers["XGB"] = XGBClassifier(max_depth=50,estimators=200,learning_rate=0.001)
    classifiers["RF"] = sklearn.ensemble.RandomForestClassifier(n_estimators=200)
    classifiers["MLP"] = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(2,2), random_state=1)
    classifiers["GBM"] = GradientBoostingClassifier(n_estimators=300)
    classifiers["DT"] = DecisionTreeClassifier(random_state=0)
    classifiers["SVM"] = sklearn.svm.SVC()
    classifiers["Dummy"] = DummyClassifier()
    classifiers["LDA"] = LinearDiscriminantAnalysis()
    classifiers["QDA"] = QuadraticDiscriminantAnalysis()
#    classifiers['autoML'] = autosklearn.classification.AutoSklearnClassifier()
    
    return classifiers

def evaluate_learners(df, learners):

    learner_scores = []

    trainset = df[df['year'] < 6]
    testset = df[df['year'] == 6]

    train_target = trainset['status']
    train_train = trainset.drop(['status','year','plant'],axis=1).fillna(df.min())

    test_target = testset['status']
    test_train = testset.drop(['status','year','plant'],axis=1).fillna(df.mean())    
    
    for name,clf in learners.items():
        try:
            clf.fit(train_train,train_target)
            pred = clf.predict(test_train)
            score = sklearn.metrics.f1_score(test_target, pred, average='binary')
            acc = sklearn.metrics.accuracy_score(test_target, pred)
            learner_scores.append({"learner" : name,"accuracy":acc,"F1":score,'classifier_object' : clf})
        except:
            print("Classifier {} failed.".format(name))

    return (train_target,train_train,test_target,test_train,learner_scores)

def explain_predictions( tta, ttr, tet, tetr, classification_results):

    fnames = list(ttr.columns) ## feature names
    explainer = lime.lime_tabular.LimeTabularExplainer(ttr, feature_names=fnames, class_names=["Healthy","Disease"], discretize_continuous=False)
    print("Features used:", fnames)
    to_check = []
    for ind, x in enumerate(list(tet)):
        to_check.append(ind)

    aggregates_tmp = defaultdict(list)
    aggregates_positives_tmp = defaultdict(list)
    aggregates = {}
    aggregates_positives = {}
    for trained_classifier in classification_results:
        classifier_name = trained_classifier['learner']
        clf = trained_classifier['classifier_object']
        predictions = clf.predict(tetr)
        print(len(predictions),tet.shape)
        for j in to_check:
            try:
                prediction = predictions[j]
                real = tet.iloc[j]
                if real == prediction and real == 1:
                    
                    exp = explainer.explain_instance(tetr.iloc[j], clf.predict_proba, num_features=10, top_labels=1)
                    for x,y in exp.as_list(label=1):
                        try:
                            if x not in aggregates:
                                aggregates_tmp[x] = [y]
                            else:
                                aggregates_tmp[x].append(y)
                        except:
                            pass

                    exp.save_to_file("results/checks/"+str(real)+"_example_"+trained_classifier['learner']+"_example_"+str(j)+".html")
            except:
                pass
            try:        
                if real == prediction and real == 0:
                    
                    exp = explainer.explain_instance(tetr.iloc[j], clf.predict_proba, num_features=10, top_labels=1)
                    for x,y in exp.as_list(label=0):
                        try:
                            if x not in aggregates:
                                aggregates_positives_tmp[x] = y
                            else:
                                aggregates_positives_tmp[x].append(y)
                        except:
                            pass
            except:
                print ("Could not explain {}".format(trained_classifier['learner']))
                pass

        for k,v in aggregates_positives_tmp.items():
            aggregates_positives[k] = np.mean(v)
        for k,v in aggregates_tmp.items():
            aggregates[k] = np.mean(v)
            
        dfx = pd.DataFrame()
        dfx = dfx.append(aggregates,ignore_index=True).T.reset_index()
        dfx.columns = ["gene","Importance for disease development"]
        dfx['phenotype'] = "Disease"
        
        dfx2 = pd.DataFrame()
        dfx2 = dfx2.append(aggregates_positives,ignore_index=True).T.reset_index()
        dfx2.columns = ["gene","Importance for disease development"]
        dfx2['phenotype'] = "Healthy"

        dfx_final = pd.concat([dfx,dfx2],axis=0)
        print(dfx_final)
        dfx_final=dfx_final.rename(columns = {'phenotype':'Status'})
        gx = (ggplot(dfx_final, aes('gene', 'Importance for disease development',fill="Status"))
              + geom_bar(stat="identity") + theme_classic()
              +xlab("Gene")
              +ylab("Average importance")
              +ggtitle("Feature importances for {}".format(classifier_name))
              + theme_bw()
              + theme(axis_text_x=element_text(rotation=90, hjust=1))
              + theme(axis_text=element_text(size=12))
              + theme(axis_title=element_text(size=15))
        )
        gx.draw()
        if classifier_name == "GBM":
            plt.show()

if __name__ == "__main__":

    fnames = glob2.glob("data/preprocessed_split/*")
    final_frame = parse_datasets(fnames)
    classifier_set = construct_learner_set()
    final_frame.to_csv("datasets/trte.csv")
    print("initiating classification")
    tta, ttr, tet, tetr, results_of_classification = evaluate_learners(final_frame,classifier_set)
    cols = ["learner","accuracy","F1"]
    
    
    edf = pd.DataFrame()
    edf = edf.append(results_of_classification,ignore_index=True)
    edf.drop(columns=["classifier_object"])
    print(edf)
    explain_predictions( tta, ttr, tet, tetr, results_of_classification)
