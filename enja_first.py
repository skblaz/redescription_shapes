import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import lime
#from lime.lime_tabular import LimeTabularExplainer
import shap


def prepare_data(data):
    data = data.drop(data.columns[0], axis=1).fillna(data.mean())
    data = data.drop(['plant'], axis=1)

    grapes = {}
    target_names = np.array(["healthy", "diseased"])
    grapes["target_names"] = target_names

    target = np.array(data["status"])
    grapes["target"] = target

    feature_names = np.array(
        ["VvWRKY", "VvCKO", "VvOLP", "VvSAMT", "VvHP", "VvACYT", "VvLOX", "VvSUSY", "VvGLC2", "VvDMR6", "VvGLC3",
         "VvINV2", "VvAGPL", "VvADH1", "VvGLC1", "VvCASY"])
    grapes["feature_names"] = feature_names

    data = data.drop(['status', 'year'], axis=1)

    data_array = []
    for i in range(0, target.shape[0]):
        a = np.array([x for x in data.iloc[i]])
        data_array.append(a)
    data_array = np.array(data_array)

    dataframe = pd.DataFrame.from_records(data_array)
    dataframe.columns = feature_names
    grapes["data"] = dataframe
    #assert np.isnan(grapes["data"]).any() == False
    return grapes

def classifiers_set():

    classifiers = {}
    classifiers["RFC"] = RandomForestClassifier(n_estimators=500)
    return classifiers

def evaluate_classifiers(classifiers, X_train, X_test, y_train, y_test):

    scores = []

    for name in classifiers:
        clf = classifiers[name]
        clf.fit(X_train, y_train)
        pred = clf.predict(X_test)
        #print("Values:      {}".format(y_test))
        #print("Predictions: {}".format(pred))
        acc = sklearn.metrics.accuracy_score(y_test, pred)
        f1 = sklearn.metrics.f1_score(y_test, pred, average='binary')
        #print("Test set accuracy score: {:.2f}".format(acc))
        #print("Test set F1 score: {:.2f}".format(f1))
        scores.append({"learner": name, "accuracy": acc, "F1": f1, 'classifier_object': clf})

    return scores

def lime_explainer(classifiers_set, X_train, X_test, y_train, y_test):

    for name, clf in classifiers_set.items():

        explainer = LimeTabularExplainer(X_train, feature_names=grapes["feature_names"], class_names=grapes["target_names"],
                                         discretize_continuous=False)
        print("Features used:", grapes["feature_names"])

        i = 0
        exp = explainer.explain_instance(X_test[i], clf.predict_proba, num_features=5, top_labels=1)
        print(exp.as_list())
        print(exp.show_in_notebook(show_table=True))

def shap_explainer(classifiers_set, X_train, X_test, y_train, y_test):

    # print the JS visualization code to the notebook
    #shap.initjs()
    
    # train a RF classifier
    # zdruzimo podatke
    X = pd.concat([X_train,X_test],axis=0)
    Y = np.concatenate((y_train,y_test))

    ## natreniramo
    import xgboost
    model = xgboost.train({"learning_rate": 0.01}, xgboost.DMatrix(X, label=Y), 100)

    ## razlozimo celi dataset
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(X)

    # visualize the first prediction's explanation
    shap.summary_plot(shap_values,X)
#    shap.force_plot(explainer.expected_value, shap_values, X)
    
    ## odvisnost?
#    shap.dependence_plot("VvOLP", shap_values, X)

    ## tole je treba usposobit!
    #interactions = shap.TreeExplainer(model).shap_interaction_values(X)

    
if __name__ == "__main__":

    data_raw = pd.read_csv("datasets/trte.csv")
    grapes = prepare_data(data_raw) # grapes: data dictionary
    X_train, X_test, y_train, y_test = train_test_split(grapes["data"], grapes["target"], random_state=123)
#    classifiers_set = classifiers_set()
#    classifiers_scores = evaluate_classifiers(classifiers_set, X_train, X_test, y_train, y_test)
#    df = pd.DataFrame()
#    df = df.append(classifiers_scores, ignore_index=True)
#    df = df.drop(columns=["classifier_object"])
    #df = df.sort_values(by=['F1'])
#    print(df)


    ### LIME explainer
    #lime_explainer(classifiers_set, X_train, X_test, y_train, y_test)

    classifiers_set = None

    ### SHAP explainer
    shap_explainer(classifiers_set, X_train, X_test, y_train, y_test)
